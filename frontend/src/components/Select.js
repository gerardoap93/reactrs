import './Select.css'


const Select = ({label, ...rest}) =>{

    const {name} = {...rest}

    switch (name) {

        case "comwareProject":
            console.log("entrando a comwareProject")
            return(

                <div>
                    <label>{label}</label>
                    <select {...rest}>
                        <option {...rest.value = ""}>--Seleccione--</option>
                        <option {...rest.value = "FixtureDesign"}> Fixture Design </option>
                        <option {...rest.value = "ATE"}> ATE</option>
                        <option {...rest.value = "FixtureProgramming"}> Fixture Programming </option>
                        
                    </select>
                    
                </div>
            )
        
        case "sales":
            console.log("entrando a sales")
            return(

                <div>
                    <label>{label}</label>
                    <select {...rest}>
                        <option {...rest.value = ""}>--Seleccione--</option>
                        <option {...rest.value = "JRG"}> JRG </option>
                        <option {...rest.value = "JD"}> JD</option>
                        <option {...rest.value = "RF"}> RF </option>
                        <option {...rest.value = "AA/JRG"}> AA/JRG </option>
                        <option {...rest.value = "JD/AA"}> JD/AA </option>
                        <option {...rest.value = "RF/AA"}> RF/AA </option>
                        <option {...rest.value = "GZ/AA"}> GZ/AA </option>
                        <option {...rest.value = "JD/JRG"}> JD/JRG </option>
                        <option {...rest.value = "RF/JRG"}> RF/JRG </option>
                        <option {...rest.value = "GZ/JRG"}> GZ/JRG </option>
                        <option {...rest.value = "JD/RF"}> JD/RF </option>
                        <option {...rest.value = "GZ/RF"}> GZ/RF </option>
                        <option {...rest.value = "EUA"}>EUA </option>
                        <option {...rest.value = "RM"}> RM </option>
                        <option {...rest.value = "JRG/RM"}> JRG/RM </option>
                        <option {...rest.value = "LP"}> LP </option>
                        <option {...rest.value = "LP/JRG"}> LP/JRG </option>
                        <option {...rest.value = "LP/RF"}> LP/RF </option>
                        <option {...rest.value = "IG/JRG"}> IG/JRG </option>
                        <option {...rest.value = "JRG/GZ"}> JRG/GZ </option>
                        <option {...rest.value = "EUA/JRG"}> EUA/JRG </option>
                        
                    </select>
                    
                </div>
            )

        case "statusQuote":
            console.log("entrando a statusQuote")
            return(
    
                <div>
                    <label>{label}</label>
                    <select {...rest}>
                    <option {...rest.value = ""}>--Seleccione--</option>
                    <option {...rest.value = "OnGoing"}> On going </option>
                    <option {...rest.value = "Done"}> Done </option>
                    <option {...rest.value = "Hold"}> Hold </option>
                    <option {...rest.value = "Dead"}> Dead </option>
                    <option {...rest.value = "PO"}> PO </option>``
                            
                </select>
                        
            </div>
            )
        


        case "customer":
            console.log("entrando a customer")
            return(
    
                <div>
                    <label>{label}</label>
                    <select {...rest}>
                    <option {...rest.value = ""}>--Seleccione--</option>
                    <option {...rest.value = "flexsouth"}> Flex south </option>
                    <option {...rest.value = "asteelflashca"}> Asteel flash CA </option>
                    <option {...rest.value = "asteelflashtj"}> Asteel flash TJ </option>
                    <option {...rest.value = "aptiv"}> Aptive </option>
                    <option {...rest.value = "benchmarkgdl"}> Benchmark GDL </option>
                    <option {...rest.value = "bosetijuana"}> Bose Tijuana </option>
                    <option {...rest.value = "cisco"}> Cisco </option>
                    <option {...rest.value = "cohu"}> Cohu </option>
                    <option {...rest.value = "continentalnogales"}> Continental nogales </option>
                    <option {...rest.value = "continentaltijera"}> Continental tijera </option>
                    <option {...rest.value = "continentalperiferico"}> Continental periferico </option>
                    <option {...rest.value = "creston"}> Creston </option>
                    <option {...rest.value = "commscope"}> Commscope </option>
                    <option {...rest.value = "designep"}> Design_EP </option>
                    <option {...rest.value = "digitalconcept"}> Digital concept </option>
                    <option {...rest.value = "dycsa"}> Dycs </option>
                    <option {...rest.value = "ego"}> Ego </option>
                    <option {...rest.value = "feasa"}> Feasa </option>
                    <option {...rest.value = "flexnorth"}> Flex North </option>
                    <option {...rest.value = "flexaguas"}> Flex Aguas </option>
                    <option {...rest.value = "flexjuarez"}> Flex Juarez </option>
                    <option {...rest.value = "gpv"}> GPV </option>
                    <option {...rest.value = "hella"}> Hella </option>
                    <option {...rest.value = "honeywell"}> Honeywell </option>
                    <option {...rest.value = "ibtest"}> Ibtest </option>
                    <option {...rest.value = "ikor"}> Ikor</option>
                    <option {...rest.value = "imigdl"}> IMI GDL</option>
                    <option {...rest.value = "Interlatin"}> Interlatin</option>
                    <option {...rest.value = "inventuspower"}> Inventus power</option>
                    <option {...rest.value = "jabilchihuahua"}> Jabil chihuahua</option>
                    <option {...rest.value = "jabiltech"}> Jabil technology</option>
                    <option {...rest.value = "jabilvaldepenas"}> Jabil valdepeñas</option>  
                    <option {...rest.value = "jabilmonterrey"}> Jabil monterrey</option>
                    <option {...rest.value = "kostalacambaro"}> Kostal Acambaro</option>
                    <option {...rest.value = "liteon"}> Liteon</option>
                    <option {...rest.value = "leaderst"}> Leaderst </option>
                    <option {...rest.value = "lmco"}> LMCO</option>
                    <option {...rest.value = "magictech"}> Magic tech</option>
                    <option {...rest.value = "magna"}> Magna</option>
                    <option {...rest.value = "microtest"}> Microtest</option>
                    <option {...rest.value = "masterworks"}> Master works</option>
                    <option {...rest.value = "melecsqueretaro"}> Melecs queretaro</option>
                    <option {...rest.value = "mgintegration"}> MG integration</option>
                    <option {...rest.value = "neotechjrz"}> Neotech juarez</option>
                    <option {...rest.value = "neotechtj"}> Neotech TJ</option>
                    <option {...rest.value = "nxstagetj"}> Nxstage TJ</option>
                    <option {...rest.value = "omron"}> Omron</option>
                    <option {...rest.value = "panasonicmonterrey"}> Panasonic monterrey</option>
                    <option {...rest.value = "plexus"}> Plexus</option>
                    <option {...rest.value = "pyramid"}> Pyramid</option>
                    <option {...rest.value = "qa"}> QA</option>
                    <option {...rest.value = "reckonsolutions"}> Reckon solutions</option>
                    <option {...rest.value = "regiotoolings"}> Regio toolings</option>
                    <option {...rest.value = "sanminapl1"}> Sanmina PL1</option>
                    <option {...rest.value = "sanminapl2"}>  Sanmina PL2</option>
                    <option {...rest.value = "sanminapl3"}>  Sanmina PL3</option>
                    <option {...rest.value = "sanminapl6"}>  Sanmina PL6</option>
                    <option {...rest.value = "sanminareynosa"}> Sanmina reynosa</option>
                    <option {...rest.value = "sigmatron"}> Sigmatron</option>
                    <option {...rest.value = "simplesolutions"}> Simple solutions</option>
                    <option {...rest.value = "stonedrige"}> Stonedrige</option>
                    <option {...rest.value = "valeoqueretaro"}> Valeo queretaro</option>
                    <option {...rest.value = "vectralis"}> Vectralis</option>
                    <option {...rest.value = "viavisolutions"}> Viavi solutions</option>
                    <option {...rest.value = "visteonchihuahua"}> Visteon chihuahua</option>
                    <option {...rest.value = "Yasakidurango"}> Yasaki durango</option>
                    <option {...rest.value = "winstron"}> Winstron</option>
                    <option {...rest.value = "vimex"}> Vimex</option>
                    <option {...rest.value = "usi"}> USI</option>
                    <option {...rest.value = "testinghouse"}> Testing house</option>
                    <option {...rest.value = "sidec"}> Sidec</option>
                    <option {...rest.value = "salcomp"}>Salcomp</option>
                    <option {...rest.value = "resideo"}> Resideo</option>
                    <option {...rest.value = "kostal"}> Kostal</option>
                    <option {...rest.value = "ieee"}> IEEE</option>
                    <option {...rest.value = "gdlcircuits"}> GDL circuits</option>
                    <option {...rest.value = "harman"}> Harman</option>
                    <option {...rest.value = "harmantj"}> Harman TJ</option>
                    <option {...rest.value = "foxconnjrz"}> Foxconn Jrz</option>
                    <option {...rest.value = "foxconngdl"}> Foxconn GDL</option>
                    <option {...rest.value = "forwesson"}> Forwerssun</option>
                    <option {...rest.value = "fceo"}> FCEO</option>
                    <option {...rest.value = "ect"}> ECT</option>
                    <option {...rest.value = "celestica"}> Celestica</option>
                    <option {...rest.value = "aztroniks"}> Aztroniks</option>
                    <option {...rest.value = "arcadia"}> Arcadia</option>
                    <option {...rest.value = "firstronic"}> Firstronic</option>
                    <option {...rest.value = "molex"}> Molex</option>
                    <option {...rest.value = "trimble"}> Trimble</option>
                    <option {...rest.value = "smto"}> SMTO</option>
                    <option {...rest.value = "diehl"}> Diehl</option>
                    <option {...rest.value = "melecs"}> Melecs</option>
                    <option {...rest.value = "hillrom"}> Hill-rom</option>
                    <option {...rest.value = "panasonic"}> Panasonic</option>
                    <option {...rest.value = "gentherm"}> Gentherm</option>
                    <option {...rest.value = "nidec"}> Nidec</option>
                    <option {...rest.value = "Bojay"}> Bojay</option>
                    <option {...rest.value = "mexicantest"}> Mexican test</option>
                    <option {...rest.value = "enphase"}> Enphase</option>
                    <option {...rest.value = "valeo"}> Valeo</option>
                    <option {...rest.value = "carrierutc"}> Carrier UTC</option>
                    <option {...rest.value = "macktechnologies"}> Mack technologies</option>
                    <option {...rest.value = "masterpiece"}> Master piece</option>
                    <option {...rest.value = "visteoncarolinaschi"}> Visteon carolinas chihuahua</option>
                    <option {...rest.value = "valeoelectronicsystem"}> Valeo electronic systems- Rio bravo</option>
                    <option {...rest.value = "strattecjrzpl1"}> Strattech Jrz planta1</option>
                    <option {...rest.value = "honeywelljrz"}> Honeywell sensing and control Jrz</option>
                    <option {...rest.value = "continentaljrz"}> Continental Jrz</option>
                    <option {...rest.value = "cooperpowersystemsjrz"}> Cooper power systems Jrz</option>
                    <option {...rest.value = "delphi"}> Delphi mexico technical center</option>
                    <option {...rest.value = "bosetj"}> Bose corporation TJ</option>
                    
                
                </select>
                        
            </div>
            )
        default:
            console.log("entrando a algo diferente =>",  typeof(name))
            break;

    }

}

export default Select