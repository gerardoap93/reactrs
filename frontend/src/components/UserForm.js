import Select from './Select'
import Button from './Button'
import Input from './Input'
import useFormulario from '../hooks/useFormulario'

const UserForm = ({submit}) => {


    const [formulario, handleChange, reset] = useFormulario({
        comwareProject:'', 
        sales:'',
        customer:'',
        contactName: '',
        projectClassification: '',
        modelName: '',
        statusQuote:'',
        tracking: '',
        poNumber: '',
        poDay: '',
        poMonth: '',
        poYear: '',
        amount: '',
        invoiceNumber: ''
      })

      const handleSubmit = (e) => {
        e.preventDefault()
        submit(formulario)
        reset()
      }
    return (

        <form onSubmit={handleSubmit}>

        <Select
        label="Comware Project:"
        name="comwareProject"
        value={formulario.comwareProject}
        onChange={handleChange}     
        />

        <Select
        label="Sales:"
        name="sales"
        value={formulario.sales}
        onChange={handleChange}          
        />


        <Select 
          label="Customer:" 
          name="customer" 
          value={formulario.customer} 
          onChange={handleChange}
          placeholder="..."
          />
        
        <Input
          label="Contact Name:"
          name="contactName"
          value={formulario.contact}
          onChange={handleChange}
          placeholder="..."
        />

        <Input
          label="Project classification:"
          name="projectClassification"
          value={formulario.projectClassification}
          onChange={handleChange}
          placeholder="..."
        />
        
        <Input
          label="Model name:"
          name="modelName"
          value={formulario.modelName}
          onChange={handleChange}
          placeholder="..."
        />
        
        <Select
          label="Status quote:"
          name="statusQuote"
          value={formulario.statusQuote}
          onChange={handleChange}
          placeholder="..."
        />

        <Input
          label="Tracking:"
          name="tracking"
          value={formulario.tracking}
          onChange={handleChange}
          placeholder="..."
        />

        <Input
          label="Po number:"
          name="poNumber"
          value={formulario.poNumber}
          onChange={handleChange}
          placeholder="..."
        />

        <Input
          label="Po day:"
          name="poDay"
          value={formulario.poDay}
          onChange={handleChange}
          placeholder="..."
        /> 

        <Input
          label="Po month:"
          name="poMonth"
          value={formulario.poMonth}
          onChange={handleChange}
          placeholder="..."
        /> 

        <Input
          label="Po year:"
          name="poYear"
          value={formulario.poYear}
          onChange={handleChange}
          placeholder="..."
        /> 

        <Input
          label="Amount:"
          name="amount"
          value={formulario.amount}
          onChange={handleChange}
          placeholder="..."
        /> 

        <Input
          label="Invoice number:"
          name="invoiceNumber"
          value={formulario.invoiceNumber}
          onChange={handleChange}
          placeholder="..."
        />     

          <Button>
            Enviar
          </Button>

      </form>
    )
}

export default UserForm