
import Card from './components/Card'
import Container from './components/Container'
import React, {useState} from 'react'
import UserForm from './components/UserForm'

function App() {
  const [usuarios, setUsuarios] = useState([])
  // const [message, setMessage] = useState([]);

  // const getWelcomeMessage = async () => {
  //   const requestOptions = {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   };
  //   const response = await fetch("/items/4", requestOptions);
  //   const data = await response.json();

  //   if (!response.ok) {
  //     console.log("something messed up")
  //   } else {
  //     setMessage(data.message)
  //   }
  //   console.log(data)
  // };
  




  const submit = usuario => {
    setUsuarios([
      ...usuarios,
      usuario,
    ])

    fetch("http://192.168.0.158:8000/items", {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(usuario)
    }).then(() =>{
      console.log("new item added")
    })


    
    // console.log("enviste formulario") 
  }

  console.log(usuarios)

  return (
    <div style={{marginTop: '15%'}}>

    
      <Container>
        <Card>
          <div style={{padding:10}}>
            <UserForm submit={submit}/>
          </div>
        </Card>
      </Container>
    </div>

  );
}


export default App

