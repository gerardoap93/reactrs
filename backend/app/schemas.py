from pydantic import BaseModel

class ItemRequestModel(BaseModel):
    comwareProject : str
    sales : str
    customer : str
    contactName : str
    projectClassification: str
    modelName : str
    statusQuote : str
    tracking : str
    poNumber : str
    poDay : str
    poMonth : str
    poYear : str
    amount : str
    invoiceNumber: str

class ItemResponseModel(ItemRequestModel):
    id: int
    