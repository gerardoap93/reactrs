from fastapi import FastAPI
from database import database as connection
from database import Item
from schemas import ItemRequestModel
from fastapi import HTTPException
from schemas import ItemResponseModel
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(title= 'lIVE',
description="prueba fast api",
version="1.0.1")

# arquitectura REST
# get, post, put, delete 

# fast api nos permite trabajar con la libreria pydantic para validar los
# datos de entrada y los datos de salida

# pydantic -> libreria que nos permite validar los valores de un modelo apartir
# de las anotaciones

# oaut2 credentials o http credentials en fast api para contrasenas seguras

origins = [
    'http://192.168.0.158:3000',
    'http://localhost:3000'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

##########################   INITIAL AND SHUTDOW CONNECTION  EVENTS ########################## 
@app.on_event('startup')
def startup():
    if connection.is_closed():
        connection.connect()
       
    connection.create_tables([Item])
        

@app.on_event('shutdown')
def shutdown():
    if not connection.is_closed():
        connection.close()

##########################    PRINCIPAL API WEB PAGE    ########################## 
@app.get('/')
async def index():
    return "hola comware con fast api"

##########################     POST FUNCTIONS DATABASE    ########################## 
@app.post('/items')
async def create_item(item_request: ItemRequestModel):
    item = Item.create(
        comwareProject=item_request.comwareProject,
        sales=item_request.sales,
        customer=item_request.customer,
        contactName=item_request.contactName,
        projectClassification=item_request.projectClassification,
        modelName=item_request.modelName,
        statusQuote = item_request.statusQuote,
        tracking = item_request.tracking,
        poNumber = item_request.poNumber,
        poDay = item_request.poDay,
        poMonth = item_request.poMonth,
        poYear = item_request.poYear,
        amount = item_request.amount,
        invoiceNumber = item_request.invoiceNumber

    )

    return item_request
##########################    GET FUNCTIONS DATABASE    ########################## 
@app.get('/items/{item_id}')
async def get_item(item_id):

    a = Item.select().where(item_id == Item.id).first()
    
    if a:
        return ItemRequestModel(id=a.id, 
        comwareProject=a.comwareProject,
        sales=a.sales,
        customer = a.customer,
        contactName = a.contactName,
        projectClassification = a.projectClassification,
        modelName = a.modelName,
        statusQuote = a.statusQuote,
        tracking = a.tracking,
        poNumber = a.poNumber,
        poDay = a.poDay,
        poMonth = a.poMonth,
        poYear = a.poYear,
        amount = a.amount,
        invoiceNumber = a.invoiceNumber
        )

    else:
        return HTTPException(404, 'Item not found')

##########################    DELETE FUNCTIONS DATABASE    ########################## 
@app.delete('/items/{item_id}')
async def delete_item(item_id):

    a = Item.select().where(item_id == Item.id).first()
    
    if a:
        a.delete_instance()
        return True

    else:
        return HTTPException(404, 'Item not found')



