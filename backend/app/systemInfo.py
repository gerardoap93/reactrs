import socket

def getIpAddress():
    hostname = socket.gethostname()
    ipAddress = socket.gethostbyname(hostname)

    return ipAddress
