from peewee import *
from config import PASSWORD


database = MySQLDatabase(
    'comware-data',
    user='root', password=PASSWORD,
    host="172.20.0.5", port=3306
)


class Item(Model):
    comwareProject = CharField(max_length=50)
    sales = CharField(max_length=50)
    customer = CharField(max_length=50)
    contactName = CharField(max_length=50)
    projectClassification=CharField(max_length=50)
    modelName = CharField(max_length=50)
    statusQuote = CharField(max_length=50)
    tracking = CharField(max_length=50)
    poNumber = CharField(max_length=50)
    poDay = CharField(max_length=50)
    poMonth = CharField(max_length=50)
    poYear = CharField(max_length=50)
    amount = CharField(max_length=50)
    invoiceNumber = CharField(max_length=50)

    def __str__(self):
        return self.comwareProject

    class Meta:
        database = database
        table_name = 'comwareProjects'

